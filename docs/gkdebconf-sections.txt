<SECTION>
<FILE>files</FILE>
sort_configs
get_options
pkg_info
sections
</SECTION>

<SECTION>
<FILE>debconf</FILE>
check_missing
get_frontends
fe_select
sig_hand
run_config
</SECTION>

<SECTION>
<FILE>cbs</FILE>
configure_cb
</SECTION>

<SECTION>
<FILE>config</FILE>
write_config
read_config
remember_fe_cb
show_libgnome_missing_alert
</SECTION>

<SECTION>
<FILE>interface</FILE>
create_main_window
create_menubar
create_gkdebconf_image
show_about_window
gk_dialog
set_description
gtk_list_store_set_strings
list_item_selected
cf_select
gk_splash
about_cb
gk_dialog_for_gconf
</SECTION>

