/*
 * GkDebconf -- Help to configure packages with debconf
 * Copyleft (C) 2003 Agney Lopes Roth Ferraz <agney@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stdio.h>
#include <string.h>
#include <gtk/gtk.h>
#include <gtk/gtkenums.h>


#include "../config.h"
#include "defines.h"
#include "interface.h"
#include "files.h"
#include "debconf.h"
#include "config.h"
#include "cbs.h"

extern GtkWidget *main_window;
GtkWidget *text;
GList *pkgsec_list;
GList *pkgdesc_list;
/* package list to configure */
GtkListStore *store_cf;
GtkWidget *gtklist_cf;
GtkWidget *splash_label;

/**
 * create_main_window:
 *
 * This function create the main window:
 * fills package names and order them by sections
 * fills desciptions
 * fills frontends
 *
 * Returns: Pointer to the window created
 */

GtkWidget*
create_main_window (void)
{
  GtkWidget *win;
  GtkWidget *vbox;
  GtkWidget *menubar;
  GtkWidget *hbox;
  GtkWidget *hbox_buttons;
  GtkWidget *vbox_choose;
  GtkWidget *scrolled_window;
  GtkWidget *scrolled_window_cf;
  GtkWidget *frame_list;
  GtkWidget *label_frame;
  GtkListStore *store_sec;
  GtkWidget *gtklist_sec;
  GtkTreeSelection *select_sec;
  GtkTreeSelection *select_cf;
  GtkCellRenderer *cell;
  GtkTreeViewColumn *column;
  GtkWidget *frame_desc;
  GtkWidget *label_desc;
  GtkWidget *scrolled_window_desc;
  GtkWidget *frame;
  GtkWidget *label_choose;
  GtkWidget *toplabel;
  GtkWidget *ok_btn;
  GtkWidget *cancel_btn;
  GdkPixbuf *icon_pixbuf;
  GList *cflist;
  GList *sec_list;
  GList **pkgs_infos;
  GError *error = NULL;

  /* Get Info */

  cflist = get_options();
  pkgs_infos = pkg_info(cflist);
  pkgsec_list = pkgs_infos[0];
  pkgdesc_list = pkgs_infos[1];
  sec_list = sections(pkgsec_list);

  win = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW(win), "GkDebconf");
  gtk_window_set_position (GTK_WINDOW(win), GTK_WIN_POS_CENTER);
  gtk_window_set_default_size(GTK_WINDOW(win), 550, 480);
  g_signal_connect (G_OBJECT (win), "delete-event", gtk_main_quit, NULL);

  vbox = gtk_box_new (GTK_ORIENTATION_VERTICAL, 2);
  gtk_widget_show (vbox);

  gtk_container_add (GTK_CONTAINER(win), vbox);

  menubar = create_menubar (win);

  icon_pixbuf
    = gdk_pixbuf_new_from_file ("/usr/share/pixmaps/gkdebconf-icon.png",
                                &error);
  if (!icon_pixbuf)
    {
      fprintf (stderr, "Failed to load pixbuf file: %s.\n%s\n",
               "gkdebconf-icon.png", error->message);
      g_error_free (error);
    }
  if (icon_pixbuf)
    {
      gtk_window_set_icon (GTK_WINDOW (win), icon_pixbuf);
      g_object_unref (icon_pixbuf);
    }

  /* generate frontend's in Options menu */
  get_frontends(menubar);
  gtk_widget_show (menubar);
  gtk_box_pack_start (GTK_BOX(vbox), menubar, FALSE, FALSE, 0);

  hbox = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 1);
  gtk_widget_show (hbox);

  gtk_box_pack_start (GTK_BOX(vbox), hbox, TRUE, TRUE, 0);

  scrolled_window = gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
                                  GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

  gtk_widget_set_size_request (scrolled_window, 120, 150);
  gtk_widget_show(scrolled_window);

  /* Sections list */
  frame_list = gtk_frame_new (NULL);
  gtk_container_set_border_width (GTK_CONTAINER(frame_list), 0);
  gtk_widget_show (frame_list);

  label_frame = gtk_label_new(_("Sections"));
  gtk_widget_show (label_frame);
  gtk_frame_set_label_widget (GTK_FRAME (frame_list), label_frame);
  gtk_label_set_justify (GTK_LABEL (label_frame), GTK_JUSTIFY_LEFT);

  store_sec = gtk_list_store_new (1, G_TYPE_STRING);
  gtklist_sec = gtk_tree_view_new_with_model (GTK_TREE_MODEL (store_sec));
  gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(gtklist_sec), FALSE);
  cell = gtk_cell_renderer_text_new ();
  column = gtk_tree_view_column_new_with_attributes ("Sections ", cell,
           "text", 0, NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (gtklist_sec),
                               GTK_TREE_VIEW_COLUMN (column));

  gtk_container_add (GTK_CONTAINER(scrolled_window), gtklist_sec);

  select_sec = gtk_tree_view_get_selection (GTK_TREE_VIEW(gtklist_sec));
  gtk_tree_selection_set_mode (select_sec, GTK_SELECTION_SINGLE);

  g_signal_connect (G_OBJECT (select_sec), "changed",
                    G_CALLBACK (list_item_selected), cflist);

  /* fill with section names */
  gtk_list_store_set_strings (GTK_LIST_STORE(store_sec), sec_list);

  gtk_widget_show (gtklist_sec);

  gtk_container_add (GTK_CONTAINER (frame_list), scrolled_window);
  gtk_box_pack_start (GTK_BOX(hbox), frame_list, TRUE, TRUE, 2);

  /* Description */
  frame_desc = gtk_frame_new (NULL);
  gtk_container_set_border_width (GTK_CONTAINER(frame_desc), 2);
  gtk_widget_show (frame_desc);

  label_desc = gtk_label_new(_("Package description"));
  gtk_widget_show (label_desc);
  gtk_frame_set_label_widget (GTK_FRAME (frame_desc), label_desc);
  gtk_label_set_justify (GTK_LABEL (label_desc), GTK_JUSTIFY_LEFT);

  text = gtk_text_view_new();
  gtk_text_view_set_editable(GTK_TEXT_VIEW(text), FALSE);
  gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(text), GTK_WRAP_WORD);

  set_description(text, _("Please select a package."));
  gtk_widget_show(text);

  scrolled_window_desc = gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window_desc),
                                  GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);
  gtk_container_add (GTK_CONTAINER(scrolled_window_desc), text);
  gtk_widget_show(scrolled_window_desc);

  gtk_container_add (GTK_CONTAINER (frame_desc), scrolled_window_desc);

  gtk_box_pack_start (GTK_BOX(vbox), frame_desc, TRUE, TRUE, 0);

  /* useful stuff */
  frame = gtk_frame_new (NULL);
  gtk_container_set_border_width (GTK_CONTAINER(frame), 0);
  gtk_widget_show (frame);

  label_choose = gtk_label_new(_("Select package"));
  gtk_widget_show (label_choose);
  gtk_frame_set_label_widget (GTK_FRAME (frame), label_choose);
  gtk_label_set_justify (GTK_LABEL (label_choose), GTK_JUSTIFY_LEFT);

  vbox_choose = gtk_box_new (GTK_ORIENTATION_VERTICAL, 1);
  gtk_container_add(GTK_CONTAINER(frame), vbox_choose);
  gtk_widget_show(vbox_choose);

  toplabel = gtk_label_new (_("... and click \"Configure\" button"));
  gtk_label_set_justify (GTK_LABEL(toplabel), GTK_JUSTIFY_LEFT);
  gtk_widget_show (toplabel);

  scrolled_window_cf = gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window_cf),
                                  GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);

  /* Package list */
  store_cf = gtk_list_store_new (1, G_TYPE_STRING);
  gtklist_cf = gtk_tree_view_new_with_model (GTK_TREE_MODEL (store_cf));
  gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(gtklist_cf), FALSE);

  cell = gtk_cell_renderer_text_new ();
  column = gtk_tree_view_column_new_with_attributes ("Packages", cell,
           "text", 0, NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (gtklist_cf),
                               GTK_TREE_VIEW_COLUMN (column));

  gtk_container_add (GTK_CONTAINER(scrolled_window_cf), gtklist_cf);

  select_cf = gtk_tree_view_get_selection (GTK_TREE_VIEW(gtklist_cf));
  gtk_tree_selection_set_mode (select_cf, GTK_SELECTION_SINGLE);
  g_signal_connect(G_OBJECT (select_cf), "changed",
                   G_CALLBACK (cf_select), cflist);

  // Don't know how to make configure_cb function called when doubleclick on package in list
  //  g_signal_connect(G_OBJECT (select_cf), "double_click_event",
  //               GTK_SIGNAL_FUNC(configure_cb), GTK_TREE_SELECTION(select_cf));

  gtk_widget_show (gtklist_cf);
  gtk_widget_show(scrolled_window_cf);

  /* fill with package names */

  gtk_list_store_set_strings (GTK_LIST_STORE(store_cf), cflist);

  gtk_box_pack_start(GTK_BOX(vbox_choose), toplabel, FALSE, FALSE, 2);

  //  gtk_box_pack_start(GTK_BOX(vbox_choose), gtklist_cf, TRUE, TRUE, 2);
  gtk_box_pack_start(GTK_BOX(vbox_choose), scrolled_window_cf, TRUE, TRUE, 0);
  gtk_box_pack_start (GTK_BOX(hbox), frame, TRUE, TRUE, 2);

  hbox_buttons = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 1);
  gtk_widget_show (hbox_buttons);

  ok_btn = gtk_button_new_from_icon_name ("gtk-execute", GTK_ICON_SIZE_BUTTON);
  gtk_button_set_label (GTK_BUTTON (ok_btn), (_("_Configure")));
  gtk_button_set_use_underline (GTK_BUTTON (ok_btn), TRUE);
  gtk_button_set_always_show_image (GTK_BUTTON (ok_btn), TRUE);
  g_signal_connect (G_OBJECT (ok_btn), "clicked",
                    G_CALLBACK (configure_cb), GTK_TREE_SELECTION (select_cf));
  gtk_widget_show (ok_btn);

  cancel_btn = gtk_button_new_with_mnemonic (_("Quit"));
  g_signal_connect (G_OBJECT (cancel_btn), "clicked",
                    gtk_main_quit, NULL);
  gtk_widget_show (cancel_btn);

  gtk_box_pack_start(GTK_BOX(hbox_buttons), cancel_btn, TRUE, TRUE, 2);
  gtk_box_pack_start(GTK_BOX(hbox_buttons), ok_btn, TRUE, TRUE, 2);

  gtk_box_pack_start(GTK_BOX(vbox), hbox_buttons, FALSE, FALSE, 2);

  //  gtk_list_select_item(GTK_LIST(list), 0);

  return win;
}


/**
 * create_menubar:
 * @win: The window where the menubar will be
 *
 * Check if the last frontend should be remembered and what is it
 *
 * Returns: A widget with a menu bar filled.
 */


GtkWidget*
create_menubar (GtkWidget *win)
{

  GtkWidget *menubar;
  GtkWidget *menu;
  GtkWidget *item;
  GtkWidget *file_menu;
  GtkWidget *opts_menu;
  GtkWidget *help_menu;
  GtkAccelGroup *accel_group;
  gchar *remember_fe = NULL;

  accel_group = gtk_accel_group_new ();
  gtk_window_add_accel_group (GTK_WINDOW(win), accel_group);
  menubar = gtk_menu_bar_new ();

  file_menu = gtk_menu_new ();
  menu = gtk_menu_item_new_with_mnemonic (_("_File"));
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (menu), file_menu);
  item = gtk_menu_item_new_with_mnemonic (_("_Quit"));
  g_signal_connect (item, "activate", gtk_main_quit, NULL);
  gtk_widget_add_accelerator (item, "activate", accel_group, GDK_KEY_q,
                              GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);
  gtk_menu_shell_append (GTK_MENU_SHELL (file_menu), item);
  gtk_menu_shell_append (GTK_MENU_SHELL (menubar), menu);
  gtk_widget_show (menu);
  gtk_widget_show (item);

  opts_menu = gtk_menu_new ();
  /* Store this object for easy access when populating the menu with
     get_frontends.  */
  g_object_set_data (G_OBJECT (menubar), "frontends", opts_menu);
  menu = gtk_menu_item_new_with_mnemonic (_("_Options"));
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (menu), opts_menu);
  item = gtk_check_menu_item_new_with_mnemonic (_("_Remember last Frontend"));
  remember_fe = read_config ("remember-frontend");
  if (!strcmp (remember_fe, "yes"))
    gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (item), TRUE);
  g_free (remember_fe);
  gtk_menu_shell_append (GTK_MENU_SHELL (opts_menu), item);
  gtk_menu_shell_append (GTK_MENU_SHELL (menubar), menu);
  gtk_widget_show (menu);
  gtk_widget_show (item);
  item = gtk_separator_menu_item_new ();
  gtk_menu_shell_append (GTK_MENU_SHELL (opts_menu), item);
  gtk_widget_show (item);

  help_menu = gtk_menu_new ();
  menu = gtk_menu_item_new_with_mnemonic (_("_Help"));
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (menu), help_menu);
  item = gtk_menu_item_new_with_mnemonic (_("_About"));
  g_signal_connect (item, "activate", G_CALLBACK (about_cb), NULL);
  gtk_menu_shell_append (GTK_MENU_SHELL (help_menu), item);
  gtk_menu_shell_append (GTK_MENU_SHELL (menubar), menu);
  gtk_widget_show (menu);
  gtk_widget_show (item);

  return menubar;
}

/**
 * create_gkdebconf_image:
 * @iname: The file name that contains the image.
 *
 * Returns: A widget with the image in.
 */

GtkWidget*
create_gkdebconf_image (gchar *iname)
{
  GtkWidget *image;
  gchar *pixmap_file;

#ifndef PREFIX
#define PREFIX "/usr"
#endif

  pixmap_file = g_strdup_printf ("%s/share/gkdebconf/%s", PREFIX, iname);
  image = gtk_image_new_from_file (pixmap_file);
  g_free (pixmap_file);

  return image;
}

/**
 * show_about_window:
 * @mainwin: A pointer to main window.
 * @msg: The about message.
 * @iname: The file name that contains the image.
 *
 * This funcion just show about windows under main window. And on close
 * back focus to main window.
 */

void
show_about_window (GtkWidget *mainwin, gchar *msg, gchar *iname)
{
  GtkWidget *about_win;
  GtkWidget *hbox;
  GtkWidget *image;
  GtkWidget *about_label;

  about_win = gtk_dialog_new_with_buttons ("GkDebconf", GTK_WINDOW(mainwin),
              GTK_DIALOG_MODAL,
                                           (_("_Close")),
              GTK_RESPONSE_CLOSE,
              NULL);

  gtk_window_set_position (GTK_WINDOW(about_win), GTK_WIN_POS_CENTER);

  /* box */
  hbox = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 2);
  gtk_container_add (GTK_CONTAINER (gtk_dialog_get_content_area
                                    (GTK_DIALOG (about_win))),
                     hbox);

  /* image */
  image = create_gkdebconf_image(iname);
  gtk_box_pack_start (GTK_BOX(hbox), image, TRUE, TRUE, 2);

  /* label */
  about_label = gtk_label_new (msg);
  gtk_label_set_justify (GTK_LABEL(about_label), GTK_JUSTIFY_LEFT);
  gtk_box_pack_start (GTK_BOX(hbox), about_label, TRUE, TRUE, 2);

  gtk_widget_show_all (about_win);

  gtk_dialog_run (GTK_DIALOG(about_win));
  gtk_widget_destroy (about_win);
}

/**
 * gk_dialog:
 * @type: Type of the message.
 * @format: The format of type.
 *
 * Shows 'msg' in a dialog box with an OK button
 * This function is to be a helper for functions needing to
 * display some information to the user
 *
 */

void
gk_dialog (GtkMessageType type, gchar *format, ...)
{
  GtkWidget *diag_win;

  va_list ap;
  gchar *msg;

  va_start(ap, format);
  msg = g_strdup_vprintf(format, ap);
  va_end(ap);

  diag_win = gtk_message_dialog_new (NULL, GTK_DIALOG_MODAL,
                                     type, GTK_BUTTONS_CLOSE,
                                     "%s",msg);

  g_signal_connect (diag_win, "delete-event", gtk_main_quit, NULL);
  gtk_window_set_position (GTK_WINDOW(diag_win), GTK_WIN_POS_CENTER);
  gtk_window_set_resizable (GTK_WINDOW(diag_win), FALSE);

  gtk_widget_show_all (diag_win);
  gtk_dialog_run (GTK_DIALOG(diag_win));

  g_free (msg);

  gtk_widget_destroy (diag_win);
}

/**
 * set_description:
 * @text: the gtk_text_view where description will be showed.
 * @pkg: The package description.
 *
 *
 */

void set_description(GtkWidget *text, gchar *pkg)
{
  GtkTextBuffer *buffer;

  buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(text));
  gtk_text_buffer_set_text(buffer, pkg, -1);
  gtk_text_view_set_buffer(GTK_TEXT_VIEW(text), buffer);
}

/**
 * gtk_list_store_set_strings:
 * @store: The gtkListStore Object that will be filled.
 * @list: Glist with data.
 *
 *
 */

void gtk_list_store_set_strings (GtkListStore *store, GList *list)
{
  GtkTreeIter iter;
  GList *node = g_list_copy(list);
  gtk_list_store_clear(store);
  while (node != NULL)
    {
      gtk_list_store_append(store, &iter);
      gtk_list_store_set(store, &iter, 0, node->data, -1);
      node = node->next;
    }
}

/**
 * list_item_selected:
 * @selection: GtkTreeSelection that will receive data.
 * @data: Glist with data that will be loaded on selection.
 *
 * Section list selection callback.
 *
 */

void list_item_selected(GtkTreeSelection *selection, gpointer data)
{
  GtkTreeIter iter;
  GtkTreeModel *model;
  GtkTreePath *path;

  GList *cf_items = NULL;
  gchar *item_data_string;
  gchar *str, *p;
  gint i = 0;
  GList *cflist = (GList *) data;
  GList *pkgsec_list_copy = g_list_copy(pkgsec_list);

  if (gtk_tree_selection_get_selected (selection, &model, &iter))
    {
      gtk_tree_model_get (model, &iter, 0, &item_data_string, -1);

      while (pkgsec_list_copy)
        {
          str = pkgsec_list_copy->data;

          if (strcmp(str, item_data_string) == 0)
            {
              p = g_list_nth_data(cflist, i);
              cf_items = g_list_append(cf_items, p);
            }

          pkgsec_list_copy = pkgsec_list_copy->next;
          i++;
        }
    }
  if(g_list_length(cf_items) == 0)
    cf_items = g_list_copy(cflist);

  gtk_list_store_set_strings (GTK_LIST_STORE(store_cf), cf_items);

  path = gtk_tree_path_new_first();
  gtk_tree_view_set_cursor(GTK_TREE_VIEW (gtklist_cf),path, NULL,FALSE);

}

/**
 * cf_select:
 * @selection: GtkTreeSelection that will receive data.
 * @data: Glist with data that will be loaded on selection.
 *
 * Finds package from list with g_list_find_custom and
 * selects package desciption from pkgdesc_list
 *
 */

void cf_select(GtkTreeSelection *selection, gpointer data)
{

  GtkTreeIter iter;
  GtkTreeModel *model;
  gchar *package;
  gint selected;
  GList *cflist = (GList *) data;

  if (gtk_tree_selection_get_selected (selection, &model, &iter))
    {
      gtk_tree_model_get (model, &iter, 0, &package, -1);
      if (strlen(package) == 0)
        return;
      selected = g_list_position(cflist, g_list_find_custom(cflist,package,sort_configs));
      set_description(text, g_list_nth_data (pkgdesc_list, selected));
      g_free (package);
    }

}

/**
 * gk_splash:
 *
 * Show gkdebonf splash image and a laber under the image
 * showing status.
 */

GtkWidget*
gk_splash ()
{
  GtkWidget *win;
  GtkWidget *vbox;

  GtkWidget *image;

  win = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_container_set_border_width (GTK_CONTAINER(win), 3);
  gtk_window_set_position (GTK_WINDOW(win), GTK_WIN_POS_CENTER);
  gtk_window_set_decorated (GTK_WINDOW(win), FALSE);

  /* box */
  vbox = gtk_box_new (GTK_ORIENTATION_VERTICAL, 2);
  gtk_widget_show (vbox);

  gtk_container_add (GTK_CONTAINER(win), vbox);

  image = create_gkdebconf_image("gkdebconf-splash.xpm");
  gtk_widget_show (image);

  gtk_box_pack_start (GTK_BOX(vbox), image, TRUE, TRUE, 2);

  splash_label = gtk_label_new (_("\n Starting GkDebconf... \n"));
  gtk_label_set_justify (GTK_LABEL (splash_label), GTK_JUSTIFY_CENTER);
  gtk_widget_show (splash_label);

  gtk_box_pack_start (GTK_BOX(vbox), splash_label, TRUE, TRUE, 2);

  return win;
}

/**
 * about_cb:
 * @w:
 * @data:
 *
 */

void
about_cb (GtkWidget *w, gpointer data)
{
  char msg[1024];

  sprintf (msg,
           _("GkDebconf %s\n\n"
             "This program is intended for those who are not\n"
             "used to the Debian packaging system and don't\n"
             "know how to find what packages can be reconfigured\n"
             "and how to reconfigure them.\n\n"
             "Authors:\n\n"
             "Agney Lopes Roth Ferraz <agney@users.sourceforge.net> \n"
             "Gustavo Noronha Silva <kov@debian.org>\n\n"
             "GUI improvements for version 1.2 by:\n\n"
             "Mantas Kriauciunas <mantas@akl.lt> \n"
             "Martynas Jocius <mjoc@delfi.lt> \n"
            ),
           VERSION);

  show_about_window (main_window, msg, "gkdebconf-splash.xpm");
}

/**
 * gk_dialog_for_gconf:
 * @key:
 * @format:
 *
 */

void
gk_dialog_for_gconf (  gchar *key, gchar *format, ... )
{
  GtkWidget *libgnome_alert_dialog;
  GtkWidget *dialog_vbox_libgnome_alert;
  GtkWidget *vbox_libgnome_alert;
  GtkWidget *message;
  GtkWidget *show_message_again;
  GtkWidget *closebutton_libgnome_alert;
  GdkPixbuf *icon_pixbuf;
  GError *error = NULL;


  va_list ap;
  gchar *msg;

  va_start(ap, format);
  msg = g_strdup_vprintf(format, ap);
  va_end(ap);


  libgnome_alert_dialog = gtk_dialog_new ();
  gtk_window_set_title (GTK_WINDOW (libgnome_alert_dialog), _("Alert"));
  gtk_window_set_position (GTK_WINDOW (libgnome_alert_dialog), GTK_WIN_POS_CENTER);
  gtk_window_set_resizable (GTK_WINDOW (libgnome_alert_dialog), FALSE);

  icon_pixbuf
    = gdk_pixbuf_new_from_file ("/usr/share/pixmaps/gkdebconf-icon.png",
                                &error);
  if (!icon_pixbuf)
    {
      fprintf (stderr, "Failed to load pixbuf file: %s: \n %s\n",
               "/usr/share/pixmaps/gkdebconf-icon.png", error->message);
      g_error_free (error);
    }
  else
    {
      gtk_window_set_icon (GTK_WINDOW (libgnome_alert_dialog), icon_pixbuf);
      g_object_unref (icon_pixbuf);
    }


  dialog_vbox_libgnome_alert
    = gtk_dialog_get_content_area (GTK_DIALOG (libgnome_alert_dialog));
  gtk_widget_show (dialog_vbox_libgnome_alert);

  vbox_libgnome_alert = gtk_box_new (GTK_ORIENTATION_VERTICAL, 7);
  gtk_widget_show (vbox_libgnome_alert);
  gtk_box_pack_start (GTK_BOX (dialog_vbox_libgnome_alert), vbox_libgnome_alert, FALSE, FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (vbox_libgnome_alert), 4);

  message = gtk_label_new ( msg );

  gtk_box_pack_start (GTK_BOX (vbox_libgnome_alert), message, TRUE, FALSE, 0);
  gtk_label_set_justify (GTK_LABEL (message), GTK_JUSTIFY_LEFT);

  show_message_again = gtk_check_button_new_with_mnemonic (_("Do not show this message again."));
  gtk_widget_show (show_message_again);
  gtk_box_pack_start (GTK_BOX (vbox_libgnome_alert), show_message_again, FALSE, FALSE, 0);

  closebutton_libgnome_alert = gtk_button_new_with_mnemonic (_("Close"));
  gtk_widget_show (closebutton_libgnome_alert);
  gtk_dialog_add_action_widget (GTK_DIALOG (libgnome_alert_dialog), closebutton_libgnome_alert, GTK_RESPONSE_CLOSE);
  gtk_widget_set_can_default (closebutton_libgnome_alert, TRUE);

  g_signal_connect (libgnome_alert_dialog, "delete-event",
                    G_CALLBACK (gtk_widget_destroy),
                    NULL);
  g_signal_connect (closebutton_libgnome_alert, "clicked",
                    G_CALLBACK (gtk_widget_destroy),
                    NULL);

  gtk_widget_show_all(libgnome_alert_dialog);
  gtk_dialog_run (GTK_DIALOG (libgnome_alert_dialog));

  g_free(msg);
  if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (show_message_again)))
    write_config (key, "no");

  gtk_widget_destroy (libgnome_alert_dialog);


}
