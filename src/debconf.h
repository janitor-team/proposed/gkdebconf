/*
 * GkDebconf -- Help to configure packages with debconf
 * Copyleft (C) 2003 Agney Lopes Roth Ferraz <agney@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _DEBCONF_H_

typedef struct __dfrontend
  {
    gchar *name; /* front end's name */
    gushort need_term;
    gchar *dep1;  /* first package that provides frontend */
    gchar *dep2; /*  another package that provides the same frontend, if exist*/
  }
dfrontend;

void get_debconf();
int check_missing (gchar *fe);
void get_frontends (GtkWidget *combo);
void fe_select (GtkMenuItem *entry, gpointer user_data);
void sig_hand (gint sig);
int run_config (gchar *cf);

#endif
